FROM node:alpine

WORKDIR /app

COPY package.json /app

ENV PATH /app/node_modules/.bin:$PATH

RUN npm install --silent
RUN npm install react-scripts@3.4.1 -g --silent

RUN yarn install

COPY . /app

CMD ["npm", "start", "app.js"]